<?php /* Template Name: Speakers */ ?>

<!-- speakers -->
    <div class="section speakers-container" id="speakers">
      <div class="container">
        <div class="row">
          <div class="col-md-12 padded">
            <h1><?php the_field('h1_title'); ?></h1>
          </div>
        </div>
        <div class="row">
          <!-- speaker-buttons -->
          <div class="col-md-4" id="speakers-left">
            <ul class="media-list" role="tablist">
              <li class="active media" data-target="#chick" role="tab" data-toggle="tab" aria-controls="chick">
                <img class="media-object pull-left" src="img/chick.jpg">
                <div class="media-body">
                  <h4 class="media-heading">Chick Foxgrover</h4>
                  <p>Chief Digital Officer<br><small>American Association of Advertising Agencies</small></p>
                </div>
              </li>
              <li class="media" data-target="#gunnard" role="tab" data-toggle="tab" aria-controls="gunnard">
                <img class="media-object pull-left" src="img/gunnard.jpg">
                <div class="media-body">
                  <h4 class="media-heading">Gunnard Johnson</h4>
                  <p>SVP Data & Analytics<br><small>Centro / Ann Arbor</small></p>
                </div>
              </li>
              <li class="media" data-target="#shawn" role="tab" data-toggle="tab" aria-controls="shawn">
                <img class="media-object pull-left" src="img/shawn.png">
                <div class="media-body">
                  <h4 class="media-heading">Shawn Stevenson</h4>
                  <p>Bestselling Author<br><small>#1 Health & Wellness Podcaster</small></p>
                </div>
              </li>
              <li class="media" data-target="#gunnar" role="tab" data-toggle="tab" aria-controls="gunnar">
                <img class="media-object pull-left" src="img/gunnar.jpg">
                <div class="media-body">
                  <h4 class="media-heading">Gunnar Kiene</h4>
                  <p>Creative Director<br><small>SapientNitro</small></p>
                </div>
              </li>
              <li class="media" data-target="#pollmann" role="tab" data-toggle="tab" aria-controls="pollmann">
                <img class="media-object pull-left" src="img/dan-pollman.jpg">
                <div class="media-body">
                  <h4 class="media-heading">Dan Pollmann</h4>
                  <p>Founder<br><small>happyMedium</small></p>
                </div>
              </li>
            </ul>
          </div>
          <!-- speaker-content -->
          <div class="col-md-8 speakers-white-box tab-content">
            
            <div role="tabpanel" class="tab-pane fade in active" id="chick">
              <div class="white-box-right pull-right">
                <a href="https://www.linkedin.com/in/chickfoxgrover" class="social-icon" target="_blank"><i class="fa fa-2x fa-linkedin-square"></i></a>
                <a href="https://twitter.com/chickfoxgrover" class="social-icon" target="_blank"><i class="fa fa-2x fa-twitter-square"></i></a>
              </div>
              <div class="white-box-left">
                <h3>Special Guest Host</h3>
                <h4>Chick Foxgrover | Chief Digital Officer | <a href="#" target="_blank">American Association of Advertising Agencies</a></h4>
              </div>
              <hr>
              <p>Not only does Chick lead the digital acumen for the 4A's and their agency members,  he is also one of the managing partners of the highly successful CreateTech conference in NYC.  Chick will be providing some opening remarks and introducing this year’s #STLDIGSYM.  From CreateTech: Technology has created broad new fields of exploration for creativity — for products, utility and communications. It’s not enough to optimize our existing practices; we must reach across old divisions to discover new innovative opportunities for the agency, the enterprise and society.</p>
              <p><a href="http://createtech.aaaa.org/#ct2015-1" target="_blank">createtech.aaaa.org</a></p>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="gunnard">
              <div class="white-box-right pull-right">
                <a href="https://www.linkedin.com/in/gunnardj" class="social-icon" target="_blank"><i class="fa fa-2x fa-linkedin-square"></i></a>
                <a href="https://twitter.com/gunnardj" class="social-icon" target="_blank"><i class="fa fa-2x fa-twitter-square"></i></a>
              </div>
              <div class="white-box-left">
                <h4>Gunnard Johnson | SVP Data & Analytics | <a href="#" target="_blank">Centro / Ann Arbor</a></h4>
              </div>
              <hr>
              <h4>DATA</h4>
              <h5>5 Pillars of Performance – Using Data & Tools as Insights</h5>
              <p>In this presentation, Gunnard identifies and explains the Pillars of Performance; five important elements that manifest in digital media plans. These pillars – audience, channel, execution, exposure and response – work individually and together to define digital campaigns. A media team with a strong understanding of each pillar will be capable of designing comprehensive plans that drive successful campaigns.</p>
              <p>Within each pillar, Gunnard will explore how newly available data and advanced tools uncover important insights into online consumer behavior. He’ll go over how these insights connect to broader business goals, and offer best practices for turning these insights into powerful campaign results.</p>
              <hr>
              <h5>Bio:</h5>
              <p>Gunnard is currently the leader of Centro’s Data & Analytics services. He is charged with strengthening Centro’s leadership team to create a new group to drive audience measurement and advanced analytic solutions. His role is to ensure that Centro incorporates robust, easy-to-use data science and marketing attribution features that meet the demand of brand and direct marketers alike.
              In addition to his role @ Centro, Gunnard is also a Board member of the Advertising Research Foundation where he is an industry advocate for consumer insight & ad effectiveness, a frequent speaker @ ARF conferences, as well as a mentor & coach to ARF Young Pros.</p>
              <p>Gunnard was previously the Advertising Research Director for Google, where he lead the Americas team and influenced Google’s push into brand measurement. An industry leader, Gunnard has worked with the IAB to define the next generation of engagement metrics, the ARF where he has spoken at multiple conferences on topics such as brand measurement and big data, and multiple consortium efforts to improve cross-media measurement via next generation media mix and attribution models. Prior to Google, Gunnard was the SVP of Marketing Science at WPP’s TeamDetroit, where he established a global data and analytics practice for Ford and GroupM’s diversified clients. Gunnard started his career at Andersen Consulting, which became Accenture, in CRM and Customer Insights. He has an undergraduate degree in Political Science and a Master’s Degree in Industrial and Operations Engineering from the University of Michigan. Gunnard lives outside of Ann Arbor, MI with his wife and 3 boys.</p>
              <div class="panels">
                <h5>Moderator:</h5>
                <a href="https://www.linkedin.com/in/leshostetler" target="_blank">Les Hostetler (Group Search Director – Ogilvy / NEO / Purina)</a>
              </div>
              <div class="panels">
                <h5>Panelists:</h5>
                <a href="https://www.linkedin.com/in/derekmabie" target="blank">Derek Mabie (Evolve)</a>, <a href="https://www.linkedin.com/in/erinemoloney" target="_blank">Erin Moloney (Perficient)</a>, <a href="https://www.linkedin.com/in/jamierule" target="_blank">Jamie Rule (HLK)</a>, <a href="https://www.linkedin.com/in/ridhi-malhotra-03651539" target="_blank">Ridhi Malhotra (GroupM)</a>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="shawn">
              <div class="white-box-right pull-right">
                <a href="https://www.linkedin.com/pub/shawn-Stevenson/23/461/4b3" class="social-icon" target="_blank"><i class="fa fa-2x fa-linkedin-square"></i></a>
                <a href="https://twitter.com/ShawnModel" class="social-icon" target="_blank"><i class="fa fa-2x fa-twitter-square"></i></a>
              </div>
              <div class="white-box-left">
                <h4>Shawn Stevenson | Bestselling Author | #1 Health & Wellness Podcaster</h4>
              </div>
              <hr>
              <h4>CONTENT</h4>
              <h5>Content vs. Context – Finding relevance in culture, personality and timeliness</h5>
              <p>In the game of digital marketing, relevance is everything. And that means delivering innovative creative in the right time and place. We hope to explore how agencies and brands are revolutionizing the creative process to grapple with speed, channel choices, media possibilities and real-time data to win big by leveraging the real world experience of #1 iTunes fitness podcaster and TedX speaker Shawn Stevenson.</p>
              <hr>
              <h5>Bio:</h5>
              <p>Shawn Stevenson is a bestselling author and creator of The Model Health Show, featured as the #1 Nutrition and Fitness podcast on iTunes. A graduate of The University of Missouri – St. Louis with a background in biology and kinesiology, Shawn went on to be the founder of Advanced Integrative Health Alliance, a successful company that provides Wellness Services for both individuals and organizations worldwide. Shawn is also a dynamic keynote speaker who has spoken for TEDx, universities, and numerous organizations with outstanding reviews</p>
              <div class="panels">
                <h5>Moderator:</h5>
                <a href="https://www.linkedin.com/in/ryebrow" target="_blank">Ryan Brown (ECD – Manifest Digital)</a>
              </div>
              <div class="panels">
                <h5>Panelists:</h5>
                <a href="https://www.linkedin.com/pub/jessen-wabeke/8b/176/343" target="blank">Jessen Wabeke (Fusion Marketing)</a>, <a href="https://www.linkedin.com/in/justinellis" target="_blank">Justin Ellis (Fleishman Hillard)</a>, <a href="https://www.linkedin.com/in/moxmas" target="_blank">Morgan Noel (XperienceLab)</a>, <a href="https://www.linkedin.com/in/kateerker" target="_blank">Kate Erker (Mio)</a>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="gunnar">
              <div class="white-box-right pull-right">
                <a href="https://www.linkedin.com/pub/gunnar-kiene/0/590/a03" class="social-icon" target="_blank"><i class="fa fa-2x fa-linkedin-square"></i></a>
                <!-- <a href="#" class="social-icon" target="_blank"><i class="fa fa-2x fa-twitter-square"></i></a> -->
              </div>
              <div class="white-box-left">
                <h4>Gunnar Kiene | Creative Director | <a href="http://www.sapientnitro.com/" target="_blank">SapeintNitro</a></h4>
              </div>
              <hr>
              <h4>CREATIVE<h4>
              <h5>Brand Consistency vs. Cohesion in a Digital World</h5>
              <p>In our hyperconnected world, there are seemingly endless ways for a brand to interact with consumers. The sheer volume of touch points – and the power consumers now have in defining the brand –  make it increasingly difficult for organizations to maintain control of their identity.</p>
              <p>
              This is no time for rigid design systems and matching luggage. Brands must be flexible and quickly adapt to market forces to remain relevant while avoiding fragmentation.</p>
              <p>
              Gunnar will explore the implications this has on design and experience systems and how organizations can succeed in this “brand new world”.</p>
              <hr>
              <h5>Bio</h5>
              <p>With over 18 years of experience in brand communications and design, Gunnar has earned his reputation as an innovative, passionate and inspiring leader.</p>
              <p>He is currently Creative Director at SapientNitro’s New York office where he leads large multidisciplinary teams to create award winning experiences and brand strategies rooted in the digital space. Gunnar brings his broad experience to clients like MasterCard, Verizon, MSNBC, Target, Coca Cola and MetLife.</p>
              <p>Prior to joining SapientNitro in 2012, he spent many years at R/GA and AtmosphereBBDO where he helped launch numerous campaigns and digital platforms. To complement his creative skills, Gunnar armed himself with a Masters in Strategic Communications from Columbia University in 2009.</p>
              <div class="panels">
                <h5>Moderator:</h5>
                <a href="https://www.linkedin.com/in/peterdycus" target="_blank">Peter Dycus (Director of Production - HLK)</a>
              </div>
              <div class="panels">
                <h5>Panelists:</h5>
                <a href="https://www.linkedin.com/pub/jon-fahnestock/13/567/821" target="_blank">Jon Fahnestock (Maryville University)</a>, <a href="https://www.linkedin.com/in/javastl" target="_blank">Brad Hogenmiller (ZeaVision)</a>, <a href="https://www.linkedin.com/in/chriskilcullen" target="_blank">Chris Kilcullen (H&L Partners)</a>, <a href="https://www.linkedin.com/in/russelldow" target="_blank">Russell Dow (PGAV)</a>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="pollmann">
              <div class="white-box-right pull-right">
                <a href="https://www.linkedin.com/pub/dan-pollmann/2b/8a8/b94" class="social-icon" target="_blank"><i class="fa fa-2x fa-linkedin-square"></i></a>
                <a href="https://twitter.com/happymdm" class="social-icon" target="_blank"><i class="fa fa-2x fa-twitter-square"></i></a>
              </div>
              <div class="white-box-left">
                <h4>Dan Pollmann | Founder | <a href="#" target="_blank">happyMedium</a></h4>
              </div>
              <hr>
              <h4>TECHNOLOGY</h4>
              <h5>Taking Digital Ideas Into The Physical Realm </h5>
              <p>Sometimes for digital ideas to be properly brought to life ... innovation, evolution, and utility drives the idea into a more physical and tactile place.  As humans, our senses are most engaged outside of the virtual space. Learn how one of STL’s top digital technology agencies traverses the digital landscape by not limiting their thinking to a screen.</p>
              <p>happyMedium is a innovation firm.  We look to replace traditional ad spends with a website, web app, a mobile application or something that provides utility.  We imagine or engineer the ability to take an eye exam or measure how much life is left in your oil from a phone or tablet or computer.  We write the software and design physical component, printed computer boards, or set of sensors that might interact with that software.  We write / design and build things from scratch in an effort to make someone’s life easier.  We hold a couple patents and have bright minds, engineers, developers, phds and creatives on staff.  They do magical things.</p>
              <hr>
              <h5>Bio</h5>
              <p>An accomplished and experienced creative leader, Dan's expertise encompasses a variety of mediums and communication channels with a focus on digital marketing and interactive experiences. Dan strongly believes the key in connecting with consumers exists within the brand narratives we build. Dan loves learning and working with creative thinkers to find solutions that go beyond the surface, but solve real business challenges.</p>
              <p>Dan Pollmann serves as the whatever you need him to be for the North American design team from happyMedium, an in-house global agency with the creative imperative to provide custom content and design experiences across everything. With over 16 years of professional experience, Dan has developed work for clients like Orvis, Warby Paker, Edward Jones, Steamboat Ski Resorts, Nestle Purina, Anheuser-Busch, and all current happyMedium clients.</p>
              <div class="panels">
                <h5>Moderator:</h5>
                <a href="https://www.linkedin.com/in/elbrad" target="_blank">Brad Lucas (Sr. Digital Strategist – Switch)</a>
              </div>
              <div class="panels">
                <h5>Panelists:</h5>
                <a href="https://www.linkedin.com/in/thepeel" target="_blank">John Peel (2e Creative)</a>, <a href="https://www.linkedin.com/in/davidortinau" target="_blank">David Ortinau (Rendr)</a>, <a href="https://www.linkedin.com/in/jprevel" target="_blank">JP Revel (Lelander)</a>, <a href="https://www.linkedin.com/in/drezdesign" target="_blank">Matt Johnson (H&L Partners)</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>