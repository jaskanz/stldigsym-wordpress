<?php /* Template Name: Hero */ ?>

 <!-- hero -->
  <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );?>

  <div class="section" id="hero" style="background: url(<?php echo $src[0]; ?> )">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-4 text-left">
          <img class="img-responsive" src="<?php the_field('logo');?>">
        </div>
        <div class="col-sm-8 text-right">
          <h1><?php the_field('date'); ?></h1>
          <h2><?php the_field('location'); ?></h2>
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </div>

<!-- get tickets -->
  <div class="section get-tickets-bar" id="get-tickets">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <h2 class="text-center"><b><?php the_field('get_tickets'); ?></b></h2>
        </div>
        <div class="col-md-4 text-center">
          <a class="btn-get-tix btn btn-block btn-lg" href="<?php the_field('eventbrite_link'); ?>" target="_blank">GET TICKETS</a>
        </div>
      </div>
    </div>
  </div>