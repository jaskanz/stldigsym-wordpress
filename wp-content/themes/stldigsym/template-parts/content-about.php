<?php /* Template Name: About */ ?>

<!-- about section -->
    <div id="about" class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1><?php the_field('h1_title'); ?></h1>
            <h2><?php the_field('h2_about'); ?></h2>
            <?php the_content(); ?>
          </div>
        </div>
      </div>
    </div>