<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package stldigsym
 */

?>

 <!-- footer -->
    <footer class="section" id="foot">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <a href="http://adclubstl.org/" target="_blank"><img class="adcl-footer center-block" src="img/logo_adcl_footer.svg" type="image/svg+xml"></a>
          </div>
          <div class="col-sm-4 text-center">
            <p><small>Microsite designed by <a href="http://www.wearetg.com" target="_blank">Timmermann Group</a></small></p>
          </div>
          <div class="col-sm-4">
            <div class="row">
              <div class="col-md-12 hidden-lg hidden-md hidden-sm text-center">
                <a href="http://instagram.com/adclubstl" target="_blank"><i class="fa fa-2x fa-fw fa-instagram"></i></a>
                <a href="https://twitter.com/adclubstl" target="_blank"><i class="fa fa-2x fa-fw fa-twitter"></i></a>
                <a href="https://www.facebook.com/events/114686035558456" target="_blank"><i class="fa fa-2x fa-fw fa-facebook"></i></a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 hidden-xs text-right">
                <a class="top" href="#hero">Top<i class="fa fa-arrow-circle-up fa-fw fa-lg hub"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
   
<?php wp_footer(); ?>

</body>
</html>
