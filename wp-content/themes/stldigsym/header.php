<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package stldigsym
 */
?>

<!DOCTYPE html>
<html lang="en-US" itemscope itemtype="http://schema.org/WebSite" prefix="og: http://ogp.me/ns#" >
  
  <head>
    <meta prefix="og: http://ogp.me/ns#" property="og:image:url" content="http://www.stlouisdigitalsymposium.com/img/stldigsym-og.jpg" />
    <meta prefix="og: http://ogp.me/ns#" property="og:image" content="http://www.stlouisdigitalsymposium.com/img/stldigsym-og.jpg" />
    <meta prefix="og: http://ogp.me/ns#" property="og:image:type" content="image/jpg" />
    <meta prefix="og: http://ogp.me/ns#" property="og:url" content="http://www.stlouisdigitalsymposium.com/"/>
    <meta prefix="og: http://ogp.me/ns#" property="og:title" content="Ad Club St. Louis Digital Symposium 2015" />
    <meta prefix="og: http://ogp.me/ns#" property="og:description" content="Get connected, enlightened and inspired with other digital creatives, marketers, agencies and clients."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Get connected, enlightened and inspired with other digital creatives, marketers, agencies and clients.">
    <meta name="keywords" content="St Louis, Missouri, Adclub, Digital, Symposium, November, Ballpark, Village, Fox, Sports, Tickets, Stldigsym, Industry, Advertising, Ad, Amazon, Razorfish, Cooper, Creative, Adclubstl, Adclub, HLK, Crowdsource, Timmermann, Timmermanngroup, TG, Switch, Liberate, Manifest, Toky, Rodgers, Lindenwood, COC, Osborn, Barr, AAAAs, SMCSTL, 314Digital, Design, Marketing, Creative"/>
    <meta charset="UTF-8" />
    <meta name="description" content="Get connected, enlightened and inspired with other digital creatives, marketers, agencies and clients. ">

    <title>Ad Club St. Louis Digital Symposium 2015</title>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
  <!-- window scroll shift -->
    <script>
    var shiftWindow = function() { scrollBy(0, -90) };
    window.addEventListener("hashchange", shiftWindow);
    function load() { if (window.location.hash) shiftWindow(); }
    $(document).ready(load());
    $(document).ready(function(){
    $('a[href^="#"]').on('click',function (e) {
    e.preventDefault();
    var target = this.hash,
    $target = $(target);
    $('html, body').stop().animate({
    'scrollTop': $target.offset().top-90
    }, 600, 'swing', function () {
    window.location.hash = target;
    });
    });
    });
    </script>
  <!-- instagram feed -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-instagram/0.3.1/instagram.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/ig-feed.js"></script>
  <!-- google analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-45222419-1', 'stlouisdigitalsymposium.com');
    ga('send', 'pageview');
    </script>

    <?php wp_head(); ?>
  </head>

<body>

<!-- navigation -->
    <div class="navbar navbar-default navbar-fixed-top" id="main-nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="hidden-xs navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#"><img class="nav-icon" height="50px"src="<?php bloginfo('url'); ?>/wp-content/uploads/2015/11/logo_adcl.png"></a>
          <a class="pull-right btn-get-tix btn btn-default navbar-btn hidden-sm hidden-md hidden-lg" href="http://www.eventbrite.com/e/2015-st-louis-digital-symposium-presented-by-adclubstl-and-hlk-tickets-18844303820" target="_blank">GET TICKETS</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
          <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class' => 'nav navbar-nav navbar-left' ) ); ?>
          <ul class="nav navbar-right navbar-nav">
            <li>
              <a href="https://www.facebook.com/events/114686035558456" class="hidden-xs hidden-sm" target="_blank"><i class="-official fa fa-2x fa-facebook-square"></i></a>
            </li>
            <li>
              <a href="https://www.linkedin.com/grps/Advertising-Club-Saint-Louis-1783457/about?
              " class="hidden-xs hidden-sm" target="_blank"><i class="-official fa fa-2x fa-linkedin-square"></i></a>
            </li>
            <li>
              <a href="https://twitter.com/adclubstl
              " class="hidden-xs hidden-sm" target="_blank"><i class="-official fa fa-2x fa-twitter-square"></i></a>
            </li>
            <li>
              <a class="btn-get-tix btn btn-default navbar-btn" href="http://www.eventbrite.com/e/2015-st-louis-digital-symposium-presented-by-adclubstl-and-hlk-tickets-18844303820" target="_blank">GET TICKETS</a>
            </li>
          </ul>
        </div>
      </div>
    </div>