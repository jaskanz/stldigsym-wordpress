<?php /* Template Name: Front Page */ get_header(); ?>

  <?php $args = array('post_type' => 'page', 'posts_per_page' => 99, 'order_by' => 'menu_order'); ?>
  <?php $loop = new WP_Query($args); ?>
  <?php if ($loop -> have_posts()) :?>
    <?php while ($loop -> have_posts()) : $loop -> the_post();?>
      <?php get_template_part('template-parts/content', $post -> post_title); ?>
    <?php endwhile; ?>
  <?php endif; ?>

     
    <!-- agenda -->
    <div class="section about-bar" id="agenda">
      <div class="container">
        <div class="row">
          <div class="col-md-12 padded">
            <h1>What's On Tap</h1>
          </div>
        </div>
        
      <!-- agenda single -->
        <div class="row agenda-single">
          <div class="col-md-4 agenda-left">
            <h2>11:30a</h2>
          </div>
          <div class="col-md-8 agenda-right">
            <p>Doors and registration open in the Ballpark Village / Fox Sports Midwest Live lobby
            Each attendee receives one drink ticket and complimentary appetizer buffet access.  Cash bar, lunch, beer and beverage service available throughout <a href="https://twitter.com/search?q=%23STLDIGSYM&src=typd&lang=en" target="_blank">#STLDIGSYM</a>
            </p>
          </div>
        </div>
      <!-- agenda single -->
        <div class="row agenda-single">
          <div class="col-md-4 agenda-left">
            <h2>12:30p</h2>
          </div>
          <div class="col-md-8 agenda-right">
            <p>Symposium start, complimentary appetizers and a libation provided by Ad Club STL.</p>
            <h4><a href="https://www.linkedin.com/in/chickfoxgrover" target="_blank">Chick Foxgrover (Chief Digital Officer – 4A's)</a></h4>
          </div>
        </div>
      <!-- agenda single-->
        <div class="row agenda-single">
          <div class="col-md-4 agenda-left">
            <h2>12:35p</h2>
          </div>
          <div class="col-md-8 agenda-right">
            <h4><a href="https://www.linkedin.com/in/gunnardj" target="_blank">Gunnard Johnson (SVP Data & Analytics at Centro)</a> leads the “Data” session with his presentation on “5 Pillars of Performance – Using Data & Tools as Insights”</h4>
            <h4><span>Moderator:</span>  <a href="https://www.linkedin.com/in/leshostetler" target="_blank">Les Hostetler (Group Search Director – Ogilvy / NEO / Purina)</a></h4>
            <h4><span>Panelists:</span>  <a href="https://www.linkedin.com/in/derekmabie" target="blank">Derek Mabie (Evolve)</a>, <a href="https://www.linkedin.com/in/erinemoloney" target="_blank">Erin Moloney (Perficient)</a>, <a href="https://www.linkedin.com/in/jamierule" target="_blank">Jamie Rule (HLK)</a>, <a href="https://www.linkedin.com/in/ridhi-malhotra-03651539" target="_blank">Ridhi Malhotra (GroupM)</a></h4>
          </div>
        </div>
      <!-- agenda single-->
        <div class="row agenda-single">
          <div class="col-md-4 agenda-left">
            <h2>1:40p</h2>
          </div>
          <div class="col-md-8 agenda-right">
            <h4>Shawn Stevenson leads the “Content” session with a presentation on “Content vs. Context - Relevance is Everything”</h4>
            <h4><span>Moderator:</span> <a href="https://www.linkedin.com/in/ryebrow" target="_blank">Ryan Brown (ECD – Manifest Digital)</a></h4>
            <h4><span>Panelists:</span>  <a href="https://www.linkedin.com/pub/jessen-wabeke/8b/176/343" target="blank">Jessen Wabeke (Fusion Marketing)</a>, <a href="https://www.linkedin.com/in/justinellis" target="_blank">Justin Ellis (Fleishman Hillard)</a>, <a href="https://www.linkedin.com/in/moxmas" target="_blank">Morgan Noel (XperienceLab)</a>, <a href="https://www.linkedin.com/in/kateerker" target="_blank">Kate Erker (Mio)</a></h4>
          </div>
        </div>
      <!-- agenda single -->
        <div class="row agenda-single">
          <div class="col-md-4 agenda-left">
            <h2>2:50p</h2>
          </div>
          <div class="col-md-8 agenda-right">
            <p>Break for beverages, snacks and networking</p>
          </div>
        </div>
      <!-- agenda single-->
        <div class="row agenda-single">
          <div class="col-md-4 agenda-left">
            <h2>3:20p</h2>
          </div>
          <div class="col-md-8 agenda-right">
            <h4><a href="https://www.linkedin.com/pub/gunnar-kiene/0/590/a03" target="_blank">Gunnar Kiene (Creative Director at SapientNitro)</a> leads the “creative” session with his presentation on “Brand Consistency vs. Cohesion in a Digital World”</h4>
            <h4><span>Moderator:</span> <a href="https://www.linkedin.com/in/peterdycus" target="_blank">Peter Dycus (Director of Production - HLK)</a></h4>
            <h4><span>Panelists:</span> <a href="https://www.linkedin.com/in/chriskilcullen" target="_blank">Chris Kilcullen (H&L Partners)</a>, <a href="https://www.linkedin.com/in/russelldow" target="_blank">Russell Dow (PGAV)</a>, <a href="https://www.linkedin.com/pub/jon-fahnestock/13/567/821" target="_blank">Jon Fahnestock (Maryville University)</a>, <a href="https://www.linkedin.com/in/javastl" target="_blank">Brad Hogenmiller (ZeaVision)</a></h4>
          </div>
        </div>
      <!-- agenda single-->
        <div class="row agenda-single">
          <div class="col-md-4 agenda-left">
            <h2>4:25p</h2>
          </div>
          <div class="col-md-8 agenda-right">
            <h4><a href="https://www.linkedin.com/pub/dan-pollmann/2b/8a8/b94" target="_blank">Dan Pollmann (Founder at happyMedium)</a> leads the “technology” session with his presentation on “Taking Digital Ideas Into The Physical Realm“</h4>
            <h4><span>Moderator:</span> <a href="https://www.linkedin.com/in/elbrad" target="_blank">Brad Lucas (Sr. Digital Strategist – Switch) </a></h4>
            <h4><span>Panelists:</span> <a href="https://www.linkedin.com/in/thepeel" target="_blank">John Peel (2e Creative)</a>, <a href="https://www.linkedin.com/in/davidortinau" target="_blank">David Ortinau (Rendr)</a>, <a href="https://www.linkedin.com/in/jprevel" target="_blank">JP Revel (Lelander)</a>, <a href="https://www.linkedin.com/in/drezdesign" target="_blank">Matt Johnson (H&L Partners)</a></h4>
          </div>
        </div>
      <!-- agenda single-->
        <div class="row agenda-single">
          <div class="col-md-4 agenda-left">
            <h2>5:30p</h2>
          </div>
          <div class="col-md-8 agenda-right">
            <p>Conference ends</p>
            <h4>AdClubSTL and Timmermann Group host the Ultimate Keyboard Warrior Championships.  16 agency typing champs will face off in a bracket until there is only one.  The UKW 2015 champ.  All agency peeps are welcome to join and watch the action on the 50’ big screen.  Admission is free.</h4>
            <hr>
            <a href="http://www.ultimatekeyboardwarrior.com" target="_blank"><img title="#ukw2015" src="img/ukw2015-banner.jpg" class="ukw-banner"></a>
          </div>
        </div>
        
      </div>
    </div>
    
    <!-- location -->
    <div class="section light-grey-container" id="location">
      <div class="container">
        <div class="row">
          <div class="col-md-12 padded">
            <h1>Where is FOX Sports Midwest Live?</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4 white-container">
            <div class="thumbnail">
              <img src="http://www.stlouisdigitalsymposium.com/img/directions_1.jpg" class="img-responsive">
              <div class="caption">
                <h3>Ballpark Village
                <br>Fox Sports Midwest Live!</h3>
                <p>601 Clark Ave #103<br>St. Louis, MO 63103</p>
                <small>Self-Parking: FREE 2-hour parking with validation is available from 6am-9pm. Parking validation ticket must be obtained before 9pm.</small>
              </div>
            </div>
          </div>
          <div class="col-sm-8 white-container">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3117.0918933104285!2d-90.19225599999996!3d38.62376699999992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87d8b31b77647b0d%3A0xce09535033fc72d4!2sFOX+Sports+Midwest+Live*21!5e0!3m2!1sen!2sus!4v1412694975398" width="100%" frameborder="0" style="border:0" height="500px"></iframe>
          </div>
        </div>
      </div>
    </div>
    
    <!-- sponsors -->
    <div class="section off-white-container" id="sponsors">
      <div class="container">
        <!-- presented by -->
        <div class="row">
          <div class="col-md-12 padded">
            <h2>Presented By:</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <a href="http://adclubstl.org/" target="_blank"><img class="sponsor-p center-block" src="img/logo_adcl.svg" type="image/svg+xml"></a>
          </div>
          <div class="col-sm-6">
            <a href="http://hlkagency.com/" target="_blank"><img class="sponsor-p center-block" src="img/logo_hlk.svg" type="image/svg+xml"></a>
          </div>
        </div>
        <!-- gold sponsors -->
        <div class="row">
          <div class="col-md-12 padded">
            <h3>Gold Sponsors:</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <a href="http://manifest.com/" target="_blank"><img class="sponsor-g center-block" src="img/logo_manifest.svg" type="image/svg+xml"></a>
          </div>
          <div class="col-sm-4">
            <a href="http://wearetg.com/" target="_blank"><img class="sponsor-g center-block" src="img/logo_timm.svg" type="image/svg+xml"></a>
          </div>
          <div class="col-sm-4">
            <a href="http://www.liberateyourbrand.com/" target="_blank"><img class="sponsor-g center-block" src="img/logo_switch.svg" type="image/svg+xml"></a>
          </div>
        </div>
        <!-- silver sponsors -->
        <div class="row">
          <div class="col-md-12 padded">
            <h3>Silver Sponsors:</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-4 col-sm-4">
            <a href="http://www.rodgerstownsend.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_rt.svg" type="image/svg+xml"></a>
          </div>
          <div class="col-xs-4 col-sm-4">
            <a href="http://www.lindenwood.edu/" target="_blank"><img class="sponsor-s center-block" src="img/logo_lind.svg" type="image/svg+xml"></a>
          </div>
          <div class="col-xs-4 col-sm-4">
            <a href="http://toky.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_toky.svg" type="image/svg+xml"></a>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-4 col-sm-4">
            <a href="https://creativesoncall.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_coc.svg" type="image/svg+xml"></a>
          </div>
          <div class="col-xs-4 col-sm-4">
            <a href="http://evolvedigitallabs.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_evolve.svg" type="image/svg+xml"></a>
          </div>
          <div class="col-xs-4 col-sm-4">
            <a href="http://www.digitalstrike.com/" target="_blank"><img class="sponsor-s center-block" src="img/logo_ds.svg" type="image/svg+xml"></a>
          </div>
        </div>
        <!-- event sponsors -->
        <div class="row">
          <div class="col-md-12 padded">
            <h3>Event Sponsors:</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-4">
            <a href="https://www.aaaa.org/Pages/default.aspx" target="_blank"><img class="sponsor-e center-block" src="img/logo_aaaa.svg" type="image/svg+xml"></a>
          </div>
          <div class="col-xs-4">
            <a href="http://www.smcstl.com/" target="_blank"><img class="sponsor-e center-block" src="img/logo_smc.png" type="image/svg+xml"></a>
          </div>
          <div class="col-xs-4">
            <a href="http://www.314digital.com/" target="_blank"><img class="sponsor-e center-block" src="img/logo_314.svg" type="image/svg+xml"></a>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="instagram tag">
          </div>
        </div>
      </div>
    </div>

<?php get_footer(); ?>